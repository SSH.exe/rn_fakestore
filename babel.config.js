module.exports = {
  presets: ['module:@react-native/babel-preset'],
  plugins: [
    'react-native-reanimated/plugin',
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        extensions: ['.ts', '.tsx', '.js', '.ios.js', '.android.js'],
        root: ['.'],
        alias: {
          colors: './src/colors',
          components: './src/components',
          constants: './src/constants',
          enums: './src/enums',
          icons: './src/icons',
          interfaces: './src/interfaces',
          navigations: './src/navigations',
          screens: './src/screens',
          services: './src/services',
          stores: './src/stores',
          styles: './src/styles',
          src: './src',
        },
      },
    ],
  ],
};
