// modules
import {DefaultTheme} from '@react-navigation/native';

export const NavigationContainerTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#FFFFFF',
  },
};
