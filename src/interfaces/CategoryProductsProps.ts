// modules
import {NavigationProp, RouteProp} from '@react-navigation/native';
// interfaces
import {AppStackParamList} from 'interfaces/AppStackParamList.ts';

export type CategoryProductsProps = {
  route: RouteProp<AppStackParamList, 'CategoryProducts'>;
  navigation: NavigationProp<AppStackParamList, 'CategoryProducts'>;
};
