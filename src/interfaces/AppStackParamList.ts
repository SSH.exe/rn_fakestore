export type ProductProps = {
  id: number;
  category: string;
  description: string;
  image: string;
  price: number;
  rating?: {
    count: number;
    rate: number;
  };
  title: string;
};
export type AppStackParamList = {
  HOME_PAGE: undefined;
  Product: ProductProps;
  LIST_PAGE: undefined;
  CARD_PAGE: undefined;
  MORE_PAGE: undefined;
  APP_ROUTES: undefined;
  CategoryProducts: {category: string; products: ProductProps[]};
};
