export interface SvgProps {
  width?: number;
  height?: number;
  color?: string;
  focusedColor?: string;
  onPress?: () => void;
  isFocused?: boolean;
}
