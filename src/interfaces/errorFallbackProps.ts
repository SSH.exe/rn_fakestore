export type ErrorFallbackProps = {
  error: Error;
  resetError: () => void;
};
