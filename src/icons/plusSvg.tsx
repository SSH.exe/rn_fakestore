// modules
import React from 'react';
import Svg, {Circle, Path} from 'react-native-svg';
// interfaces
import {SvgProps} from 'interfaces/svgProps.ts';

function PlusSvg(props: SvgProps) {
  const {width = 30, height = 31, color = '#000', onPress} = props;
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 30 31"
      fill="none"
      onPress={onPress}>
      <Circle cx={15} cy={15.5} r={14.5} stroke={color} />
      <Path
        d="M22.057 15.498a.882.882 0 01-.882.883H15.88v5.294a.882.882 0 11-1.765 0V16.38H8.822a.882.882 0 110-1.765h5.294V9.322a.882.882 0 011.765 0v5.294h5.294c.487 0 .882.395.882.882z"
        fill={color}
      />
    </Svg>
  );
}

export default PlusSvg;
