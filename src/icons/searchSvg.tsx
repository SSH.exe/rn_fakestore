// modules
import React from 'react';
import Svg, {Path} from 'react-native-svg';
// interfaces
import {SvgProps} from 'interfaces/svgProps.ts';

function SearchSvg(props: SvgProps) {
  const {
    width = 30,
    height = 31,
    color = '#ACAAA9',
    focusedColor,
    isFocused = false,
    onPress,
  } = props;
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 30 31"
      fill="none"
      onPress={onPress}>
      <Path
        d="M20.972 12.636c0 4.17-3.307 7.523-7.352 7.523-4.046 0-7.352-3.354-7.352-7.523 0-4.17 3.306-7.523 7.352-7.523 4.045 0 7.352 3.353 7.352 7.523z"
        stroke={isFocused ? focusedColor : color}
        strokeWidth={1.5}
      />
      <Path
        d="M18.828 19.137l5.787 7.09"
        stroke={isFocused ? focusedColor : color}
        strokeWidth={1.5}
        strokeLinecap="round"
      />
    </Svg>
  );
}

export default SearchSvg;
