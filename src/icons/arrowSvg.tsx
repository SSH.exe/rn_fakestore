// modules
import React from 'react';
import Svg, {Path} from 'react-native-svg';
// interfaces
import {SvgProps} from 'interfaces/svgProps.ts';

function ArrowSvg(props: SvgProps) {
  const {
    width = 25,
    height = 25,
    color = '#3C3C3C',
    focusedColor,
    isFocused = false,
  } = props;
  return (
    <Svg width={width} height={height} viewBox="0 0 25 25" fill="none">
      <Path
        d="M9 20l7.57-6.624a.5.5 0 000-.752L9 6"
        stroke={isFocused ? focusedColor : color}
        strokeWidth={1.5}
        strokeLinecap="round"
      />
    </Svg>
  );
}

export default ArrowSvg;
