// modules
import React from 'react';
import Svg, {Path} from 'react-native-svg';
// interfaces
import {SvgProps} from 'interfaces/svgProps.ts';

function ListSvg(props: SvgProps) {
  const {
    width = 34,
    height = 34,
    color = '#000',
    focusedColor,
    isFocused = false,
  } = props;
  return (
    <Svg width={width} height={height} viewBox="0 0 34 34" fill="none">
      <Path
        d="M5.834 9.625A1.125 1.125 0 016.959 8.5h15.75a1.125 1.125 0 010 2.25H6.959a1.125 1.125 0 01-1.125-1.125zm0 15A1.125 1.125 0 016.959 23.5h14.25a1.125 1.125 0 110 2.25H6.959a1.125 1.125 0 01-1.125-1.125zM6.959 16a1.125 1.125 0 100 2.25h21.75a1.125 1.125 0 100-2.25H6.959z"
        fill={isFocused ? focusedColor : color}
      />
    </Svg>
  );
}

export default ListSvg;
