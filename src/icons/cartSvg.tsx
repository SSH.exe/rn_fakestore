// modules
import React from 'react';
import Svg, {Path} from 'react-native-svg';
// interfaces
import {SvgProps} from 'interfaces/svgProps.ts';

function CartSvg(props: SvgProps) {
  const {
    width = 34,
    height = 34,
    color = '#000',
    focusedColor,
    isFocused = false,
  } = props;
  return (
    <Svg width={width} height={height} viewBox="0 0 34 34" fill="none">
      <Path
        d="M4.23 4.967h2.414c.841 0 1.261 0 1.597.184.138.076.263.174.37.29.26.28.363.688.566 1.504l.27 1.08c.15.599.226.899.342 1.15a2.968 2.968 0 002.146 1.678c.273.05.582.05 1.2.05"
        stroke={isFocused ? focusedColor : color}
        strokeWidth={2}
        strokeLinecap="round"
      />
      <Path
        d="M25.01 24.261H9.5c-.215 0-.324 0-.407-.009a1.484 1.484 0 01-1.287-1.785c.035-.132.075-.261.12-.39.078-.228.115-.342.158-.444a2.968 2.968 0 012.52-1.817c.11-.007.232-.007.47-.007h7.999"
        stroke={isFocused ? focusedColor : color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M19.857 19.807h-5.275c-1.804 0-2.707 0-3.403-.408a2.968 2.968 0 01-.773-.656c-.52-.62-.668-1.51-.963-3.29-.302-1.803-.452-2.706-.094-3.374.149-.278.355-.522.606-.715.6-.462 1.514-.462 3.342-.462h9.879c2.152 0 3.228 0 3.664.704.435.705-.047 1.667-1.009 3.592l-.663 1.328c-.799 1.597-1.198 2.395-1.915 2.838-.718.443-1.61.443-3.396.443z"
        stroke={isFocused ? focusedColor : color}
        strokeWidth={2}
        strokeLinecap="round"
      />
      <Path
        d="M23.525 30.199a1.484 1.484 0 100-2.969 1.484 1.484 0 000 2.969zM11.65 30.199a1.484 1.484 0 100-2.969 1.484 1.484 0 000 2.969z"
        fill={isFocused ? focusedColor : color}
      />
    </Svg>
  );
}

export default CartSvg;
