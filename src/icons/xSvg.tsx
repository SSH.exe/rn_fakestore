// modules
import React from 'react';
import Svg, {Path} from 'react-native-svg';
// interfaces
import {SvgProps} from 'interfaces/svgProps.ts';

function XSVG(props: SvgProps) {
  const {
    width = 24,
    height = 24,
    color = '#322F35',
    onPress,
    isFocused,
    focusedColor,
  } = props;
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      onPress={onPress}>
      <Path
        d="M18.295 7.115a.997.997 0 10-1.41-1.41L12 10.59 7.115 5.705a.997.997 0 00-1.41 1.41L10.59 12l-4.885 4.885a.997.997 0 001.41 1.41L12 13.41l4.885 4.885a.997.997 0 101.41-1.41L13.41 12l4.885-4.885z"
        fill={isFocused ? focusedColor : color}
      />
    </Svg>
  );
}

export default XSVG;
