// modules
import React from 'react';
import Svg, {Path} from 'react-native-svg';
// interfaces
import {SvgProps} from 'interfaces/svgProps.ts';

function MoreSvg(props: SvgProps) {
  const {
    width = 34,
    height = 34,
    color = '#000',
    focusedColor,
    isFocused = false,
  } = props;
  return (
    <Svg width={width} height={height} viewBox="0 0 34 34" fill="none">
      <Path
        d="M17.5 4.834c-6.443 0-11.666 5.223-11.666 11.667 0 6.443 5.223 11.666 11.667 11.666 6.443 0 11.666-5.223 11.666-11.666 0-6.444-5.223-11.667-11.666-11.667z"
        stroke={isFocused ? focusedColor : color}
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M8.484 23.904s2.6-3.32 9.018-3.32c6.416 0 9.018 3.32 9.018 3.32M17.502 16.5a3.5 3.5 0 100-7 3.5 3.5 0 000 7z"
        stroke={isFocused ? focusedColor : color}
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default MoreSvg;
