// modules
import React from 'react';
import Svg, {G, Path, Defs, ClipPath} from 'react-native-svg';
// interfaces
import {SvgProps} from 'interfaces/svgProps.ts';

function HomeSvg(props: SvgProps) {
  const {
    width = 26,
    height = 26,
    color = '#000',
    focusedColor,
    isFocused = false,
  } = props;
  return (
    <Svg width={width} height={height} viewBox="0 0 26 26" fill="none">
      <G clipPath="url(#clip0_1496_16836)">
        <Path
          d="M13 1.333L13.542.7a.834.834 0 00-1.084 0l.542.633zm-11.667 10L.792 10.7l-.292.25v.383h.833zm8.334 13.334v.833a.833.833 0 00.833-.833h-.833zm6.666 0H15.5a.833.833 0 00.833.833v-.833zm8.334-13.334h.833v-.383l-.292-.25-.541.633zM3 25.5h6.667v-1.667H3V25.5zm22.208-14.8L13.542.7l-1.084 1.267 11.667 10 1.083-1.267zM12.458.7L.792 10.7l1.083 1.267 11.667-10L12.458.7zM10.5 24.667v-5H8.833v5H10.5zm5-5v5h1.667v-5H15.5zm.833 5.833H23v-1.667h-6.667V25.5zM25.5 23V11.333h-1.667V23H25.5zM.5 11.333V23h1.667V11.333H.5zM13 17.167a2.5 2.5 0 012.5 2.5h1.667A4.167 4.167 0 0013 15.5v1.667zm0-1.667a4.167 4.167 0 00-4.167 4.167H10.5a2.5 2.5 0 012.5-2.5V15.5zm10 10a2.5 2.5 0 002.5-2.5h-1.667a.834.834 0 01-.833.833V25.5zM3 23.833A.833.833 0 012.167 23H.5A2.5 2.5 0 003 25.5v-1.667z"
          fill={isFocused ? focusedColor : color}
        />
      </G>
      <Defs>
        <ClipPath id="clip0_1496_16836">
          <Path fill="#fff" transform="translate(.5 .5)" d="M0 0H25V25H0z" />
        </ClipPath>
      </Defs>
    </Svg>
  );
}

export default HomeSvg;
