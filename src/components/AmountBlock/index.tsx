// modules
import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
// icons
import {MinusSvg, PlusSvg} from 'icons/index.ts';
// styles
import {styles} from './styles.ts';

interface AmountBlockProps {
  img?: string;
  title?: string;
  description?: string;
  price?: string | number;
  count?: number;
  incrementFunction?: () => void;
  decrementFunction?: () => void;
  deleteFunction?: () => void;
}

function AmountBlock({
  img,
  title = 'Title',
  description = 'Description',
  price = '230.50 $',
  count = 0,
  incrementFunction,
  decrementFunction,
  deleteFunction,
}: AmountBlockProps) {
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <Image source={{uri: img}} style={styles.img} />
        <View style={styles.contents}>
          <View style={styles.body_contents}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.description}>{description}</Text>
            <Text style={styles.price}>{price}</Text>
          </View>
          <View style={styles.calculated_content}>
            <TouchableOpacity activeOpacity={0.6} onPress={decrementFunction}>
              <MinusSvg />
            </TouchableOpacity>
            <Text>{count}</Text>
            <TouchableOpacity activeOpacity={0.6} onPress={incrementFunction}>
              <PlusSvg />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.deleteButton}
        onPress={deleteFunction}>
        <Text style={styles.deleteButton_text}>Delete</Text>
      </TouchableOpacity>
    </View>
  );
}
export default AmountBlock;
