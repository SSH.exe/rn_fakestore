// modules
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {justifyContent: 'space-between', paddingTop: 25},
  img: {width: 191, height: 231, resizeMode: 'contain'},
  contents: {justifyContent: 'space-between', marginLeft: 15},
  body_contents: {gap: 15, width: 150},
  title: {color: '#000000', fontSize: 20, textAlign: 'left'},
  description: {color: '#515151', fontSize: 16, textAlign: 'left'},
  price: {color: '#000000', fontSize: 14, textAlign: 'left'},
  calculated_content: {
    marginTop: 15,
    flexDirection: 'row',
    gap: 25,
    alignItems: 'center',
  },
  deleteButton: {
    marginTop: 30,
    backgroundColor: '#999999',
    borderRadius: 3,
    alignItems: 'center',
    paddingTop: 18,
    paddingBottom: 18,
  },
  deleteButton_text: {fontSize: 16, color: '#FFFFFF', fontWeight: '500'},
});
