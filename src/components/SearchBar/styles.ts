// modules
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
  },
  searchBar__unclicked: {
    padding: 10,
    flexDirection: 'row',
    width: '100%',
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#706E6E',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  searchBar__clicked: {
    borderRadius: 5,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#D2CFCE',
    alignItems: 'center',
    padding: 10,
    flexDirection: 'row',
    width: '80%',
    justifyContent: 'space-around',
  },
  input: {
    fontSize: 20,
    marginLeft: 15,
    width: '90%',
  },
});
