// modules
import React from 'react';
import {Button, Keyboard, TextInput, View} from 'react-native';
// icons
import {SearchSvg, XSVG} from 'icons/index.ts';
// styles
import {styles} from './styles.ts';

interface SearchBarProps {
  clicked: boolean;
  searchPhrase: string;
  setSearchPhrase: (phrase: string) => void;
  setClicked: (clickStatus: boolean) => void;
}
function SearchBar({
  clicked,
  searchPhrase,
  setSearchPhrase,
  setClicked,
}: SearchBarProps) {
  return (
    <View style={styles.container}>
      <View
        style={
          clicked ? styles.searchBar__clicked : styles.searchBar__unclicked
        }>
        <SearchSvg />
        <TextInput
          style={styles.input}
          placeholder="Search"
          value={searchPhrase}
          onChangeText={setSearchPhrase}
          onFocus={() => {
            setClicked(true);
          }}
        />
        {clicked && (
          <XSVG
            color={'#ACAAA9'}
            onPress={() => {
              setSearchPhrase('');
            }}
          />
        )}
      </View>
      {clicked && (
        <View>
          <Button
            title="Cancel"
            onPress={() => {
              Keyboard.dismiss();
              setClicked(false);
            }}
          />
        </View>
      )}
    </View>
  );
}
export default SearchBar;
