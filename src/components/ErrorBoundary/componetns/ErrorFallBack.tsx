// modules
import React, {useEffect} from 'react';
import {useNavigation} from '@react-navigation/core';
import {StackNavigationProp} from '@react-navigation/stack';
import {Alert, Button, StyleSheet, Text, View} from 'react-native';
// constants
import {ScreenNames} from 'constants/screenNames.ts';
// interfaces
import {AppStackParamList, ErrorFallbackProps} from 'interfaces/index';

export default function ErrorFallBack({error, resetError}: ErrorFallbackProps) {
  const navigation = useNavigation<StackNavigationProp<AppStackParamList>>();
  const handleResetError = () => {
    resetError();
    navigation.navigate(ScreenNames.HOME_PAGE);
  };
  useEffect(() => {
    if (__DEV__) {
      Alert.alert(error.stack || 'No stack trace available');
    }
  }, [error.stack]);
  return (
    <View style={styles.mainWrapper}>
      <View style={styles.contentWrapper}>
        <Text style={styles.opsText}>Opsss. Something went wrong</Text>
        <Text style={styles.errorMessage}>Error message: {error.message}</Text>
        <Button
          onPress={() => {
            handleResetError();
          }}
          title={'Go to home'}
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  mainWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    flex: 1,
  },
  contentWrapper: {width: '100%', gap: 20},
  opsText: {fontSize: 24},
  errorMessage: {fontWeight: 'bold'},
});
