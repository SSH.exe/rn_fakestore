// modules
import React from 'react';
import {View} from 'react-native';

const ItemSeparator = ({height = 16}) => {
  return <View style={{height: height}} />;
};
export default ItemSeparator;
