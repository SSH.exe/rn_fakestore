// modules
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    padding: 10,
    alignItems: 'center',
    borderStyle: 'solid',
    borderColor: '#706E6E',
    borderWidth: 1,
    borderRadius: 12,
  },
  body: {gap: 15, marginTop: 15},
  title: {color: '#000000', fontSize: 20},
  description: {color: '#515151', fontSize: 16},
  price: {color: '#000000', fontSize: 14},
  btn: {
    marginTop: 18,
    backgroundColor: '#7D40DB',
    width: 191,
    height: 47,
    justifyContent: 'center',
  },
  buttonAdded: {
    marginTop: 18,
    backgroundColor: '#1C1A36',
    width: 191,
    height: 47,
    justifyContent: 'center',
  },
  btnText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
    color: '#FFF',
  },
});
