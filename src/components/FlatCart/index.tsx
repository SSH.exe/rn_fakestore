// modules
import React from 'react';
import {Image, PixelRatio, Text, TouchableOpacity, View} from 'react-native';
// styles
import {styles} from './styles.ts';

interface FlatCardProps {
  uri: string;
  title: string;
  price: string | number;
  description: string;
  btnText?: string;
  isAdded?: boolean;
  activeOpacity?: number;
  onPress?: () => void;
  showModal?: () => void;
}
function FlatCart({
  uri,
  title,
  price,
  description,
  isAdded,
  btnText = 'Add to cart',
  activeOpacity = 0.8,
  onPress,
  showModal,
}: FlatCardProps) {
  const maxWidth = PixelRatio.getPixelSizeForLayoutSize(191 / PixelRatio.get());
  const maxHeight = PixelRatio.getPixelSizeForLayoutSize(
    231 / PixelRatio.get(),
  );
  let titleSlice = title.length > 15 ? `${title.slice(0, 35)}...` : title;
  let numberFixed = Number(price).toFixed(2);
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={showModal}
      style={styles.container}>
      <Image
        source={{uri: uri}}
        style={{
          width: maxWidth,
          height: maxHeight,
          resizeMode: 'contain',
        }}
      />
      <View style={styles.body}>
        <Text style={styles.title}>{titleSlice}</Text>
        <Text style={styles.description} numberOfLines={1}>
          {description}
        </Text>
        <Text style={styles.price}>$ {numberFixed}</Text>
      </View>
      <TouchableOpacity
        disabled={isAdded}
        activeOpacity={activeOpacity}
        style={isAdded ? styles.buttonAdded : styles.btn}
        onPress={onPress}>
        <Text style={styles.btnText}>{isAdded ? 'Added' : btnText}</Text>
      </TouchableOpacity>
    </TouchableOpacity>
  );
}
export default FlatCart;
