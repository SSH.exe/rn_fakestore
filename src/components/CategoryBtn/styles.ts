// modules
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {backgroundColor: '#EFEFEF', justifyContent: 'center'},
  content: {
    padding: 19,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    textAlign: 'left',
    color: '#000000',
    fontSize: 15,
    fontWeight: '500',
  },
});
