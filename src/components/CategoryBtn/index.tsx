// modules
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
// icons
import {ArrowSvg} from 'icons/index.ts';
// styles
import {styles} from './styles.ts';

interface CategoryBtnProps {
  category: string;
  activeOpacity?: number;
  onPress?: () => void;
}

function CategoryBtn(props: CategoryBtnProps) {
  const {category = 'CategoryBtn', activeOpacity = 0.7, onPress} = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={activeOpacity}
        style={styles.content}
        onPress={onPress}>
        <Text style={styles.text}>{category}</Text>
        <ArrowSvg />
      </TouchableOpacity>
    </View>
  );
}

export default CategoryBtn;
