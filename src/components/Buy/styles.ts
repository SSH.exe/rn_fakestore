// modules
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    padding: 17,
    backgroundColor: '#DCDADA',
    gap: 17,
    justifyContent: 'space-around',
  },
  content: {flexDirection: 'row'},
  price: {color: '#000000', fontSize: 20, fontWeight: '500'},
  buyButton: {
    backgroundColor: '#007C2A',
    borderRadius: 3,
    alignItems: 'center',
    paddingTop: 18,
    paddingBottom: 18,
  },
  butButtonText: {color: '#FFFFFF', fontSize: 16, fontWeight: '500'},
});
