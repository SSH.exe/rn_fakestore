// modules
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
// styles
import {styles} from './styles.ts';

interface BuyProps {
  amount?: number | string;
  buyFunction?: () => void;
}
function Buy({amount = '230.50 $', buyFunction}: BuyProps) {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.price}>Total amount: </Text>
        <Text style={styles.price}>{amount}</Text>
      </View>
      <TouchableOpacity
        style={styles.buyButton}
        activeOpacity={0.8}
        onPress={buyFunction}>
        <Text style={styles.butButtonText}>Buy</Text>
      </TouchableOpacity>
    </View>
  );
}
export default Buy;
