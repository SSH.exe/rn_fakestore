// modules
import React from 'react';
import {
  Modal,
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
// interfaces
import {ProductProps} from 'interfaces/AppStackParamList.ts';
// styles
import {styles} from './styles.ts';

interface ProductModalProps {
  visible: boolean;
  isAdded?: boolean;
  onClose?: () => void;
  onAdd?: () => void;
  product?: ProductProps | null;
}

const ProductModal = ({
  visible,
  isAdded,
  onClose,
  product,
  onAdd,
}: ProductModalProps) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={onClose}>
      <TouchableWithoutFeedback onPress={onClose}>
        <View style={styles.centeredView}>
          <TouchableWithoutFeedback>
            <View style={styles.modalView}>
              <TouchableOpacity onPress={onClose} style={styles.closeButton}>
                <Text style={styles.closeButtonText}>×</Text>
              </TouchableOpacity>
              <Image source={{uri: product?.image}} style={styles.image} />
              <View style={styles.textContainer}>
                <Text style={styles.title}>{product?.title}</Text>
                <Text style={styles.description}>{product?.description}</Text>
                <Text style={styles.category}>
                  Category: {product?.category}
                </Text>
                <Text style={styles.price}>$ {product?.price}</Text>
              </View>
              <TouchableOpacity
                disabled={isAdded}
                onPress={onAdd}
                style={isAdded ? styles.buttonAdded : styles.button}>
                <Text style={styles.buttonText}>
                  {isAdded ? 'Added' : 'Add to Cart'}
                </Text>
              </TouchableOpacity>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default ProductModal;
