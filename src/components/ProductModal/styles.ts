// modules
import {StyleSheet} from 'react-native';
// constants
import {window} from 'constants/dimension.ts';

export const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalView: {
    width: window.width * 0.9,
    backgroundColor: '#FFF',
    borderRadius: 10,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    maxHeight: window.height * 0.9,
  },
  image: {
    resizeMode: 'contain',
    width: 200,
    height: 200,
    marginBottom: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
  },
  description: {
    textAlign: 'left',
  },
  category: {
    textAlign: 'left',
  },
  price: {
    textAlign: 'left',
    marginBottom: 20,
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    backgroundColor: 'transparent',
  },
  closeButtonText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000',
  },
  textContainer: {
    width: '100%',
    gap: 15,
  },
  button: {
    backgroundColor: '#7D40DB',
    borderRadius: 3,
    padding: 12,
    paddingTop: 21,
    paddingLeft: 12,
    paddingBottom: 21,
    width: '100%',
    elevation: 2,
  },
  buttonAdded: {
    backgroundColor: '#1C1A36',
    borderRadius: 3,
    padding: 12,
    paddingTop: 21,
    paddingLeft: 12,
    paddingBottom: 21,
    width: '100%',
    elevation: 2,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
