// modules
import {createNativeStackNavigator} from '@react-navigation/native-stack';
// interfaces
import {AppStackParamList} from 'interfaces/AppStackParamList.ts';

export const Stack = createNativeStackNavigator<AppStackParamList>();
