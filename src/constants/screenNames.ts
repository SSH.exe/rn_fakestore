export enum ScreenNames {
  HOME_PAGE = 'HOME_PAGE',
  LIST_PAGE = 'LIST_PAGE',
  CARD_PAGE = 'CARD_PAGE',
  MORE_PAGE = 'MORE_PAGE',
  APP_ROUTES = 'APP_ROUTES',
  Product = 'CategoryProducts',
}
