// modules
import React, {useEffect, useLayoutEffect} from 'react';
import {FlatList, View} from 'react-native';
// components
import {FlatCart, ItemSeparator, SearchBar} from 'components/index.ts';
// store
import {useAppDispatch, useAppSelector} from 'stores/index.ts';
import {clearSearch, setSearchPhrase} from 'stores/search/searchSlice.ts';
// interfaces
import {CategoryProductsProps} from 'interfaces/CategoryProductsProps.ts';
// styles
import {styles} from './styles.ts';

function CategoryProducts({route, navigation}: CategoryProductsProps) {
  const dispatch = useAppDispatch();
  const {category, products} = route.params;
  const searchPhrase = useAppSelector(state => state.search.searchPhrase);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: category,
      headerShadowVisible: false,
      headerBackTitleVisible: false,
      headerTintColor: '#322F35',
      headerTitleStyle: {
        color: '#000000',
        fontSize: 18,
      },
    });
  }, [navigation, category]);
  useEffect(() => {
    return () => {
      dispatch(clearSearch());
    };
  }, [dispatch]);

  const filteredProducts = searchPhrase
    ? products.filter(product =>
        product.title.toLowerCase().includes(searchPhrase.toLowerCase()),
      )
    : products;

  return (
    <View style={styles.container}>
      <SearchBar
        clicked={searchPhrase !== ''}
        searchPhrase={searchPhrase}
        setSearchPhrase={phrase => dispatch(setSearchPhrase(phrase))}
        setClicked={clicked =>
          clicked
            ? dispatch(setSearchPhrase(searchPhrase))
            : dispatch(clearSearch())
        }
      />
      <FlatList
        showsVerticalScrollIndicator={false}
        // data={products}
        data={filteredProducts}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => (
          <FlatCart
            uri={item.image}
            title={item.title}
            description={item.description}
            price={item.price}
          />
        )}
        ItemSeparatorComponent={ItemSeparator}
        ListHeaderComponent={ItemSeparator}
        ListFooterComponent={ItemSeparator}
      />
    </View>
  );
}

export default CategoryProducts;
