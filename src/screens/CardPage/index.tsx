// modules
import React from 'react';
import {FlatList, View} from 'react-native';
// components
import {AmountBlock, Buy} from 'components/index.ts';
// stores
import {RootState, useAppDispatch, useAppSelector} from 'stores/index.ts';
import {decrement, increment, removeItem} from 'stores/carts/cartSlice.ts';
// styles
import {styles} from './styles.ts';

function CardPage() {
  const dispatch = useAppDispatch();
  const cartItems = useAppSelector((state: RootState) => state.cart.items);
  const incrementItem = (id: number) => () => {
    dispatch(increment(id));
  };

  const decrementItem = (id: number, currentCount: number) => () => {
    if (currentCount > 1) {
      dispatch(decrement(id));
    }
  };

  const deleteItem = (id: number) => () => {
    dispatch(removeItem(id));
  };

  const totalAmount = cartItems.reduce(
    (total, item) => total + item.price * item.quantity,
    0,
  );

  return (
    <>
      <View style={styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={cartItems}
          keyExtractor={item => item.id.toString()}
          contentContainerStyle={{paddingBottom: 10}}
          renderItem={({item}) => (
            <AmountBlock
              img={item.image}
              title={item.title}
              description={item.description}
              price={`${item.price} $`}
              count={item.quantity}
              incrementFunction={incrementItem(item.id)}
              decrementFunction={decrementItem(item.id, item.quantity)}
              deleteFunction={deleteItem(item.id)}
            />
          )}
        />
      </View>
      <Buy amount={totalAmount.toFixed(2)} />
    </>
  );
}
export default CardPage;
