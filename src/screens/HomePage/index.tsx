// modules
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Keyboard,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
// components
import {
  FlatCart,
  ItemSeparator,
  ProductModal,
  SearchBar,
} from 'components/index.ts';
// interfaces
import {ProductProps} from 'interfaces/AppStackParamList.ts';
// stores
import {useAppDispatch, useAppSelector} from 'stores/index.ts';
import {addItem} from 'stores/carts/cartSlice.ts';
import {useGetProductsQuery} from 'stores/product/action.ts';
// styles
import {styles} from './styles.ts';

function HomePage() {
  const dispatch = useAppDispatch();
  const cartItems = useAppSelector(state => state.cart.items);

  const {data: products, error, isLoading} = useGetProductsQuery();
  const [displayCount, setDisplayCount] = useState<number>(3);
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [selectedProduct, setSelectedProduct] = useState<ProductProps | null>(
    null,
  );
  const [filteredProducts, setFilteredProducts] = useState<ProductProps[]>([]);
  const [isSearching, setIsSearching] = useState<boolean>(false);
  const [searchPhrase, setSearchPhrase] = useState<string>('');

  const handleLoadMore = () => {
    setDisplayCount(prevDisplayCount => prevDisplayCount + 3);
  };

  const handleOpenModal = (product: ProductProps) => {
    setSelectedProduct(product);
    setModalVisible(true);
    setIsSearching(false);
    Keyboard.dismiss();
  };

  useEffect(() => {
    setIsSearching(!!searchPhrase);
    const updatedFilteredProducts = products
      ? products.filter(
          product =>
            product.title.toLowerCase().includes(searchPhrase.toLowerCase()) ||
            String(product.id) === searchPhrase,
        )
      : [];
    setFilteredProducts(updatedFilteredProducts);
  }, [products, searchPhrase]);

  const handleAddToCart = (product: ProductProps) => {
    dispatch(addItem(product));
  };

  const renderItem = ({item}: {item: ProductProps}) => {
    const isInCart = cartItems.some(cartItem => cartItem.id === item.id);
    return (
      <FlatCart
        uri={item.image}
        title={item.title}
        description={item.description}
        price={item.price}
        isAdded={isInCart}
        showModal={() => handleOpenModal(item)}
        onPress={() => handleAddToCart(item)}
      />
    );
  };

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
        setIsSearching(false);
      }}>
      <View style={styles.content}>
        {isLoading && <Text>Loading...</Text>}
        {error && <Text>Error: {error.toString()}</Text>}
        <SearchBar
          clicked={isSearching}
          searchPhrase={searchPhrase}
          setSearchPhrase={setSearchPhrase}
          setClicked={setIsSearching}
        />
        {products && (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={filteredProducts.slice(0, displayCount)}
            keyExtractor={item => item.id.toString()}
            renderItem={({item}) => renderItem({item})}
            initialNumToRender={3}
            onEndReached={handleLoadMore}
            onEndReachedThreshold={0.5}
            ItemSeparatorComponent={ItemSeparator}
            ListHeaderComponent={ItemSeparator}
            ListFooterComponent={ItemSeparator}
          />
        )}

        <ProductModal
          isAdded={
            selectedProduct
              ? cartItems.some(item => item.id === selectedProduct.id)
              : false
          }
          visible={modalVisible}
          onClose={() => setModalVisible(false)}
          product={selectedProduct}
          onAdd={() => selectedProduct && handleAddToCart(selectedProduct)}
        />
      </View>
    </TouchableWithoutFeedback>
  );
}
export default HomePage;
