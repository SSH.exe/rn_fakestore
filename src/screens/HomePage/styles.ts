// modules
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  content: {paddingLeft: 15, paddingRight: 15, paddingBottom: 50},
});
