// modules
import React from 'react';
import {
  View,
  Text,
  FlatList,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
// components
import {CategoryBtn, ItemSeparator} from 'components/index';
// stores
import {useGetProductsQuery} from 'stores/product/action.ts';
// constants
import {ScreenNames} from 'constants/screenNames.ts';
function ListPage({navigation}: any) {
  const {data: products, error, isLoading} = useGetProductsQuery();

  const categories = products
    ? Array.from(new Set(products.map(product => product.category)))
    : [];

  const filterProductsByCategory = (category: string) => {
    return products?.filter(product => product.category === category);
  };
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={{padding: 15}}>
        {isLoading && <Text>Loading...</Text>}
        {error && <Text>Error: {error.toString()}</Text>}
        <FlatList
          scrollEnabled={false}
          horizontal={false}
          data={categories}
          keyExtractor={(item, index) => `category-${index}`}
          ItemSeparatorComponent={ItemSeparator}
          ListHeaderComponent={ItemSeparator}
          ListFooterComponent={ItemSeparator}
          renderItem={({item: category}) => (
            <CategoryBtn
              category={category}
              onPress={() => {
                const categoryProducts = filterProductsByCategory(category);
                navigation.navigate(ScreenNames.Product, {
                  category: category,
                  products: categoryProducts,
                });
              }}
            />
          )}
        />
      </View>
    </TouchableWithoutFeedback>
  );
}
export default ListPage;
