// modules
import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
// types
import {Product} from 'stores/product/model.ts';

export const productsApi = createApi({
  reducerPath: 'productsApi',
  baseQuery: fetchBaseQuery({baseUrl: 'https://fakestoreapi.com/'}),
  endpoints: builder => ({
    getProducts: builder.query<Product[], void>({
      query: () => 'products',
    }),
  }),
});

export const {useGetProductsQuery} = productsApi;
