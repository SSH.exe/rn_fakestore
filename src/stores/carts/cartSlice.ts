// modules
import {createSlice, PayloadAction} from '@reduxjs/toolkit';
// interfaces
import {CartState} from 'stores/carts/model.ts';
import {ProductProps} from 'interfaces/AppStackParamList.ts';

const initialState: CartState = {
  items: [],
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem: (state, action: PayloadAction<ProductProps>) => {
      const index = state.items.findIndex(
        item => item.id === action.payload.id,
      );
      if (index !== -1) {
        state.items[index].quantity++;
      } else {
        state.items.push({...action.payload, quantity: 1});
      }
    },
    removeItem: (state, action: PayloadAction<number>) => {
      state.items = state.items.filter(item => item.id !== action.payload);
    },
    increment: (state, action: PayloadAction<number>) => {
      const index = state.items.findIndex(item => item.id === action.payload);
      if (index !== -1) {
        state.items[index].quantity++;
      }
    },
    decrement: (state, action: PayloadAction<number>) => {
      const index = state.items.findIndex(item => item.id === action.payload);
      if (index !== -1 && state.items[index].quantity > 1) {
        state.items[index].quantity--;
      }
    },
  },
});

export const {addItem, removeItem, increment, decrement} = cartSlice.actions;

export default cartSlice.reducer;
