// modules
import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
// types
import {Cart} from 'stores/carts/model.ts';

export const cartsApi = createApi({
  reducerPath: 'cartsApi',
  baseQuery: fetchBaseQuery({baseUrl: 'https://fakestoreapi.com/'}),
  endpoints: builder => ({
    getCarts: builder.query<Cart[], void>({
      query: () => 'carts',
    }),
  }),
});

export const {useGetCartsQuery} = cartsApi;
