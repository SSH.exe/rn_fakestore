// interfaces
import {ProductProps} from 'interfaces/AppStackParamList.ts';

export interface Cart {
  id: number;
  userId: number;
  date: Date;
  products: {productId: number; quantity: number}[];
}

export interface CartItem extends ProductProps {
  quantity: number;
}

export interface CartState {
  items: CartItem[];
}
