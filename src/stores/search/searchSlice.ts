// modules
import {createSlice} from '@reduxjs/toolkit';

export const searchSlice = createSlice({
  name: 'search',
  initialState: {
    searchPhrase: '',
    isSearching: false,
  },
  reducers: {
    setSearchPhrase: (state, action) => {
      state.searchPhrase = action.payload;
      state.isSearching = !!action.payload;
    },
    clearSearch: state => {
      state.searchPhrase = '';
      state.isSearching = false;
    },
  },
});

export const {setSearchPhrase, clearSearch} = searchSlice.actions;
export default searchSlice.reducer;
