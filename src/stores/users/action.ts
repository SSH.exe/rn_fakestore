// modules
import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
// types
import {User} from 'stores/users/model.ts';

export const usersApi = createApi({
  reducerPath: 'usersApi',
  baseQuery: fetchBaseQuery({baseUrl: 'https://fakestoreapi.com/'}),
  endpoints: builder => ({
    getUsers: builder.query<User[], void>({
      query: () => 'users',
    }),
  }),
});

export const {useGetUsersQuery} = usersApi;
