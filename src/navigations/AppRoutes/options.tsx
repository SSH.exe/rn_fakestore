// modules
import React from 'react';
import {Text, Vibration, View} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {BottomTabNavigationOptions} from '@react-navigation/bottom-tabs';
// constants
import {ScreenNames} from 'constants/screenNames.ts';
// interfaces
import {TabParamList} from 'navigations/AppRoutes/index.tsx';
// icons
import {CartSvg, HomeSvg, ListSvg, MoreSvg} from 'icons/index.ts';
// styles
import {styles} from './styles.ts';

const tabNames = {
  [ScreenNames.HOME_PAGE]: 'Home',
  [ScreenNames.LIST_PAGE]: 'List',
  [ScreenNames.CARD_PAGE]: 'Cart',
  [ScreenNames.MORE_PAGE]: 'More',
};

const getIcon = (name: string, focused: boolean) => {
  switch (name) {
    case ScreenNames.HOME_PAGE:
      return <HomeSvg isFocused={focused} focusedColor={'#7D40DB'} />;
    case ScreenNames.LIST_PAGE:
      return <ListSvg isFocused={focused} focusedColor={'#7D40DB'} />;
    case ScreenNames.CARD_PAGE:
      return <CartSvg isFocused={focused} focusedColor={'#7D40DB'} />;
    case ScreenNames.MORE_PAGE:
      return <MoreSvg isFocused={focused} focusedColor={'#7D40DB'} />;
  }
};

export default function getTabOptions(
  route: RouteProp<TabParamList, keyof TabParamList>,
  cartCount: number,
): BottomTabNavigationOptions {
  const tubVibration = () => Vibration.vibrate();
  return {
    headerShown: false,
    tabBarHideOnKeyboard: true,
    tabBarShowLabel: false,
    tabBarStyle: {
      borderTopWidth: 0,
      backgroundColor: '#F0F0F0',
      height: 83,
      paddingTop: 10,
    },
    tabBarIcon: ({focused}) => {
      let optStyle = styles({focused});
      return (
        <View style={optStyle.container}>
          {getIcon(route.name, focused)}
          <Text style={optStyle.text} onPress={tubVibration}>
            {tabNames[route.name as keyof typeof tabNames]}
          </Text>
        </View>
      );
    },
    ...(route.name === ScreenNames.CARD_PAGE && {
      tabBarBadge: cartCount > 0 ? cartCount : undefined,
      tabBarBadgeStyle: {
        backgroundColor: '#7D40DB',
        color: '#FFF',
        fontSize: 11,
        marginTop: -8,
      },
    }),
  };
}
