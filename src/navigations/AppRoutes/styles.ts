// modules
import {StyleSheet} from 'react-native';
// interfaces
import {OptionsStylesProps} from 'interfaces/optionsStylesProps.ts';
export const styles = ({focused}: OptionsStylesProps) =>
  StyleSheet.create({
    container: {
      alignItems: 'center',
      gap: 5,
    },
    text: {
      color: focused ? '#7D40DB' : '#000000',
      fontSize: 11,
      fontWeight: 'bold',
    },
  });
