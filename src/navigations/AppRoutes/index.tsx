// modules
import React from 'react';
import {useCallback} from 'react';
import {RouteProp} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// constants
import {ScreenNames} from 'constants/screenNames.ts';
// screens
import {CardPage, HomePage, ListPage, MorePage} from 'screens/index.ts';
// options
import getTabOptions from 'navigations/AppRoutes/options.tsx';
// stores
import {RootState, useAppSelector} from 'stores/index.ts';

export type TabParamList = {
  [ScreenNames.HOME_PAGE]: undefined;
  [ScreenNames.LIST_PAGE]: undefined;
  [ScreenNames.CARD_PAGE]: undefined;
  [ScreenNames.MORE_PAGE]: undefined;
};
const Tab = createBottomTabNavigator<TabParamList>();

export default function AppRoutes() {
  const cartItems = useAppSelector((state: RootState) => state.cart.items);
  const cartCount = cartItems.reduce((total, item) => total + item.quantity, 0);

  const tabOptions = useCallback(
    (route: RouteProp<TabParamList, keyof TabParamList>) =>
      getTabOptions(route, cartCount),
    [cartCount],
  );
  return (
    <Tab.Navigator screenOptions={({route}) => ({...tabOptions(route)})}>
      <Tab.Screen name={ScreenNames.HOME_PAGE} component={HomePage} />
      <Tab.Screen name={ScreenNames.LIST_PAGE} component={ListPage} />
      <Tab.Screen name={ScreenNames.CARD_PAGE} component={CardPage} />
      <Tab.Screen name={ScreenNames.MORE_PAGE} component={MorePage} />
    </Tab.Navigator>
  );
}
