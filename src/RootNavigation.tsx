// modules
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
// components
import ErrorBoundary from 'components/ErrorBoundary';
// constants
import {ScreenNames} from 'constants/screenNames.ts';
import {Stack} from 'constants/stack';
// screens
import {CategoryProducts} from 'screens/index.ts';
// navigations
import AppRoutes from 'navigations/AppRoutes';
// styles
import {NavigationContainerTheme} from 'styles/rootNavigationStyles.ts';

const screenOpt = {headerShown: false};
export default function RootNavigation() {
  return (
    <NavigationContainer theme={NavigationContainerTheme}>
      <ErrorBoundary>
        <Stack.Navigator
          initialRouteName={ScreenNames.HOME_PAGE}
          screenOptions={screenOpt}>
          <Stack.Screen name={ScreenNames.APP_ROUTES} component={AppRoutes} />
          <Stack.Screen
            options={{
              headerShown: true,
            }}
            name={ScreenNames.Product}
            component={CategoryProducts}
          />
        </Stack.Navigator>
      </ErrorBoundary>
    </NavigationContainer>
  );
}
