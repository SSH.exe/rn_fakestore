import React from 'react';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {SafeAreaView, StatusBar, useColorScheme} from 'react-native';
// navigation
import RootNavigation from './src/RootNavigation.tsx';

function App(): React.JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.white,
    flex: 0,
  };
  return (
    <>
      <SafeAreaView style={backgroundStyle}>
        <StatusBar backgroundColor="transparent" translucent={true} />
      </SafeAreaView>
      <RootNavigation />
    </>
  );
}
export default App;
