/**
 * @format
 */

// modules
import React from 'react';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';
// app registry
import App from './App';
import {name as appName} from './app.json';
// store
import {store} from 'stores/index';

const Root = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => Root);
